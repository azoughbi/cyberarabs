#
# Cookbook Name:: nginx
# Recipe:: common/conf
#
# Author:: AJ Christensen <aj@junglist.gen.nz>
#
# Copyright 2008-2013, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

template 'nginx.conf' do
  path   "#{node['nginx']['dir']}/nginx.conf"
  source 'nginx.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'blacklist.conf' do
  path   "#{node['nginx']['dir']}/blacklist.conf"
  source 'blacklist.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'fastcgiparams' do
  path   "#{node['nginx']['dir']}/fastcgiparams"
  source 'fastcgiparams.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'fastcgi.conf' do
  path   "#{node['nginx']['dir']}/fastcgi.conf"
  source 'fastcgi.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'koi-utf' do
  path   "#{node['nginx']['dir']}/koi-utf"
  source 'koi-utf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'koi-win' do
  path   "#{node['nginx']['dir']}/koi-win"
  source 'koi-win.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'mime.types' do
  path   "#{node['nginx']['dir']}/mime.types"
  source 'mime.types.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'reverse_proxy.conf' do
  path   "#{node['nginx']['dir']}/reverse_proxy.conf"
  source 'reverse_proxy.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'upstream_phpapache.conf' do
  path   "#{node['nginx']['dir']}/upstream_phpapache.conf"
  source 'upstream_phpapache.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'upstream_phpcgi.conf' do
  path   "#{node['nginx']['dir']}/upstream_phpcgi.conf"
  source 'upstream_phpcgi.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'win-utf' do
  path   "#{node['nginx']['dir']}/win-utf"
  source 'win-utf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template "#{node['nginx']['dir']}/sites-available/cyber-arabs.com" do
  source 'cyber-arabs.com.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template "#{node['nginx']['dir']}/sites-available/smw" do
	source 	'syriamediawiki.org.erb'
	owner 	'root'
	group 	node['root_group']
	mode	'0644'
	notifies :reload, 'service[nginx]'
end

template 'redirect.conf' do
  path   "#{node['nginx']['iwpr']}/redirect.conf"
  source 'redirect.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

template 'allowed.conf' do
  path   "#{node['nginx']['iwpr']}/allowed.conf"
  source 'allowed.conf.erb'
  owner  'root'
  group  node['root_group']
  mode   '0644'
  notifies :reload, 'service[nginx]'
end

nginx_site 'cyber-arabs.com' do
  enable node['nginx']['default_site_enabled']
end

nginx_site 'smw' do
	enable node['nginx']['default_site_enabled']
end