#
# Cookbook Name:: fix
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

package "dmg" do
	action	:purge
end

package "aws" do
	action :purge
end

package "bluepill" do
	action :purge
end

package "runit" do
	action :purge
end